/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Job;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * FXML Controller class
 *
 * @author DELL
 */
public class JobController implements Initializable {

    @FXML
    private TableView<Job> tableView;

    @FXML
    private TableColumn<Job, String> colTitle;

    @FXML
    private TableColumn<Job, String> colCategory;

    @FXML
    private TableColumn<Job, String> colSalary;

    @FXML
    private TableColumn<Job, String> colSkill;

    @FXML
    private TableColumn<Job, Integer> colExp;

    @FXML
    private TableColumn<Job, String> colJobRole;

    @FXML
    private TableColumn<Job, String> colDescrip;

    @FXML
    private TableColumn<Job, String> colLocation;

    @FXML
    private TableColumn<Job, String> colExpDate;
    
    @FXML
    private TableColumn<Job, String> colButton;

    @FXML
    private Button btnCompany;

    @FXML
    private Button btnLogin;

    @FXML
    private ComboBox<String> comboBox;
    
    public static int JobId;

    @FXML
    void onClickCompany(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent newScene = null;

        stage = (Stage) btnCompany.getScene().getWindow();
        newScene = FXMLLoader.load(getClass().getResource("/company.fxml"));

        Scene scene = new Scene(newScene);
        stage.setScene(scene);
        stage.setTitle("Công ty");
        stage.show();
    }

    @FXML
    void onClickLogin(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent newScene = null;

        stage = (Stage) btnLogin.getScene().getWindow();
        newScene = FXMLLoader.load(getClass().getResource("/logIn.fxml"));

        Scene scene = new Scene(newScene);
        stage.setScene(scene);
        stage.setTitle("Đăng nhập");
        stage.show();
    }

    @FXML
    void onChange(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent newScene = null;

        String value = comboBox.getValue();
        switch (value) {
            case "Đăng xuất":
                LogInController.accessToken = null;
                btnLogin.setVisible(true);
                comboBox.setVisible(false);
                break;
            case "Thông tin tài khoản":
                stage = (Stage) btnLogin.getScene().getWindow();
                newScene = FXMLLoader.load(getClass().getResource("/profile.fxml"));

                Scene scene = new Scene(newScene);
                stage.setScene(scene);
                stage.setTitle("Thông tin tài khoản");
                stage.show();
                break;
        }
    }
    
    private void onClickDetail(ActionEvent event, int id) throws IOException{
        Stage stage = null;
        Parent newScene = null;
        
        JobId = id;
        
        Button btn = (Button) event.getSource();
        
        stage = (Stage) btn.getScene().getWindow();
        newScene = FXMLLoader.load(getClass().getResource("/jobDetail.fxml"));

        Scene scene = new Scene(newScene);
        stage.setScene(scene);
        stage.setTitle("Chi tiết công việc");
        stage.show();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> menu = FXCollections.observableArrayList("Thông tin tài khoản", "Đăng xuất");

        comboBox.setItems(menu);

        if (LogInController.accessToken != null) {
            btnLogin.setVisible(false);
        } else {
            comboBox.setVisible(false);
        }

        HttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:8881/jobs");
        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpGet);
        } catch (IOException ex) {
        }

        String content = null;
        try {
            content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
        } catch (IOException | UnsupportedOperationException ex) {
        }

        JSONObject responseData = new JSONObject(content);

        JSONArray arr = responseData.getJSONArray("dataList");
        
        Button[] buttons = new Button[arr.length()];
        for(int i=0; i<buttons.length; i++){
            JSONObject obj = arr.getJSONObject(i);
            
            buttons[i] = new Button();
            buttons[i].setOnAction(e -> {
                try {
                    onClickDetail(e, obj.getInt("id"));
                } catch (IOException ex) {
                    Logger.getLogger(JobController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }
        
        List<Job> jobs = new ArrayList<Job>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            Job job = new Job();
            job.setId(obj.getInt("id"));
            job.setTitle(obj.getString("title"));
            job.setCategory(obj.getString("category"));
            job.setSalary(obj.getString("salary"));
            job.setSkill(obj.getString("skill"));
            job.setExperience(obj.getInt("experience"));
            job.setJob_role(obj.getString("jobrole"));
            job.setDescription(obj.getString("description"));
            job.setLocation(obj.getString("location"));
            job.setExp_date(obj.getString("expiryDate"));
            job.setButton(buttons[i]);
            jobs.add(job);
        }
        
        ObservableList data = FXCollections.observableArrayList(jobs);

        colTitle.setCellValueFactory(new PropertyValueFactory<Job, String>("title"));
        colCategory.setCellValueFactory(new PropertyValueFactory<Job, String>("category"));
        colSalary.setCellValueFactory(new PropertyValueFactory<Job, String>("salary"));
        colSkill.setCellValueFactory(new PropertyValueFactory<Job, String>("skill"));
        colExp.setCellValueFactory(new PropertyValueFactory<Job, Integer>("experience"));
        colJobRole.setCellValueFactory(new PropertyValueFactory<Job, String>("job_role"));
        colDescrip.setCellValueFactory(new PropertyValueFactory<Job, String>("description"));
        colLocation.setCellValueFactory(new PropertyValueFactory<Job, String>("location"));
        colExpDate.setCellValueFactory(new PropertyValueFactory<Job, String>("exp_date"));
        colButton.setCellValueFactory(new PropertyValueFactory<Job, String>("button"));
        
        tableView.setItems(data);
    }

}
