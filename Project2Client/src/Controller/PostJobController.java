/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.layout.AnchorPane;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

/**
 * FXML Controller class
 *
 * @author DELL
 */
public class PostJobController implements Initializable {

    @FXML
    private TextField txtTitle;

    @FXML
    private TextField txtCategory;

    @FXML
    private TextField txtSalary;

    @FXML
    private TextField txtSkill;

    @FXML
    private TextField txtExp;

    @FXML
    private TextField txtJobRole;

    @FXML
    private TextArea txtDescrip;

    @FXML
    private TextField txtLocation;

    @FXML
    private TextField txtExpDate;

    @FXML
    private Label txtMessage;

    @FXML
    private Button btnPost;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void onClickPost(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent newScene = null;

        String title = txtTitle.getText();
        String category = txtCategory.getText();
        String salary = txtSalary.getText();
        String skill = txtSkill.getText();
        int exp = !"".equals(txtExp.getText()) ? Integer.parseInt(txtExp.getText()) : 0;
        String job_role = txtJobRole.getText();
        String description = txtDescrip.getText();
        String location = txtLocation.getText();
        String expDate = txtExpDate.getText();
        String accessToken = LogInController.accessToken;

        String body = "{\"title\": \"" + title + "\", \"category\": \"" + category + "\", \"salary\": \"" + salary + "\", \"skill\": \"" + skill + "\", \"experience\": \"" + exp + "\", \"jobrole\": \"" + job_role + "\", \"description\": \"" + description + "\", \"location\": \"" + location + "\", \"expiryDate\": \"" + expDate + "\"}";
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8881/jobs/postJob");
        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);

        try {
            StringEntity stringEntity = new StringEntity(body);
            httpPost.setEntity(stringEntity);
        } catch (UnsupportedEncodingException ex) {
        }

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpPost);
        } catch (IOException ex) {
        }

        String content = null;
        try {
            content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
        } catch (IOException | UnsupportedOperationException ex) {
        }

        JSONObject responseData = new JSONObject(content);

        if (title.isEmpty() || category.isEmpty() || salary.isEmpty() || skill.isEmpty() || job_role.isEmpty() || expDate.isEmpty()) {
            txtMessage.setText("Có lỗi xảy ra");
        } else {
//            try {
                Boolean success = responseData.getBoolean("success");
                if (success) {
                    txtMessage.setText("Đăng tin thành công");
                    stage = (Stage) btnPost.getScene().getWindow();
                    newScene = FXMLLoader.load(getClass().getResource("/Job.fxml"));

                    Scene scene = new Scene(newScene);
                    stage.setScene(scene);
                    stage.setTitle("Công việc");
                    stage.show();
                } else {
                    try {
                        txtMessage.setText("Có lỗi xảy ra");
                    } catch (Exception e) {
                    }
                }
//            } catch (Exception e) {
//                txtMessage.setText("Có lỗi xảy ra");
//            }
        }
    }

}
