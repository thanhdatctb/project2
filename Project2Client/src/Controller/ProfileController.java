/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

/**
 * FXML Controller class
 *
 * @author DELL
 */
public class ProfileController implements Initializable {

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtAvatar;

    @FXML
    private TextField txtCoverImg;

    @FXML
    private TextArea txtResume;

    @FXML
    private TextArea txtDescription;

    @FXML
    private TextField txtCompanyName;

    @FXML
    private TextField txtCompanyAvatar;

    @FXML
    private TextField txtPosition;

    @FXML
    private TextField txtPeriod;

    @FXML
    private TextArea txtExpDescription;

    @FXML
    private TextField txtRank;

    @FXML
    private Label txtMessage;

    @FXML
    private Button btnUpdate;

    @FXML
    private Button btnBack;

    @FXML
    void onClickBack(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent newScene = null;

        stage = (Stage) btnBack.getScene().getWindow();
        newScene = FXMLLoader.load(getClass().getResource("/Job.fxml"));

        Scene scene = new Scene(newScene);
        stage.setScene(scene);
        stage.setTitle("Công việc");
        stage.show();
    }

    @FXML
    void onClickUpdate(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent newScene = null;

        String name = txtName.getText();
        String resume = txtResume.getText();
        String description = txtDescription.getText();
        String avatar = txtAvatar.getText();
        String coverImg = txtCoverImg.getText();
        String companyName = txtCompanyName.getText();
        String companyAvatar = txtCompanyAvatar.getText();
        String position = txtPosition.getText();
        String period = txtPeriod.getText();
        String expDescription = txtExpDescription.getText();
        String rank = txtRank.getText();
        String accessToken = LogInController.accessToken;

        String body = "{\"name\": \"" + name + "\", \"resume\": \"" + resume + "\", \"description\": \"" + description + "\", \"avatar\": \"" + avatar + "\", \"coverImage\": \"" + coverImg + "\", \"companyName\": \"" + companyName + "\", \"companyAvatar\": \"" + companyAvatar + "\", \"position\": \"" + position + "\", \"period\": \"" + period + "\", \"expDescription\": \"" + expDescription + "\", \"rank\": \"" + rank + "\"}";
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8881/authentication/updateProfile");
        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);

        try {
            StringEntity stringEntity = new StringEntity(body);
            httpPost.setEntity(stringEntity);
        } catch (UnsupportedEncodingException ex) {
        }

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpPost);
        } catch (IOException ex) {
        }

        String content = null;
        try {
            content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
        } catch (IOException | UnsupportedOperationException ex) {
        }

        JSONObject responseData = new JSONObject(content);

        Boolean success = responseData.getBoolean("success");
        if (success) {
            txtMessage.setText("Cập nhật thành công");
        } else {
            try {
                txtMessage.setText("Có lỗi xảy ra");
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String accessToken = LogInController.accessToken;

        HttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:8881/authentication/viewProfile");
        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpGet);
        } catch (IOException ex) {
        }

        String content = null;
        try {
            content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
        } catch (IOException | UnsupportedOperationException ex) {
        }

        JSONObject responseData = new JSONObject(content);

        JSONObject data = responseData.getJSONObject("data");

        txtAvatar.setText(data.getString("avatar"));
        txtCoverImg.setText(data.getString("coverImage"));
        txtResume.setText(data.getString("resume"));
        txtDescription.setText(data.getString("description"));
        txtCompanyName.setText(data.getJSONObject("experience").getString("companyname"));
        txtCompanyAvatar.setText(data.getJSONObject("experience").getString("companyavatar"));
        txtPosition.setText(data.getJSONObject("experience").getString("position"));
        txtPeriod.setText(data.getJSONObject("experience").getString("period"));
        txtExpDescription.setText(data.getJSONObject("experience").getString("description"));
        txtRank.setText(String.valueOf(data.getJSONObject("skill").getInt("rank")));
    }

}
