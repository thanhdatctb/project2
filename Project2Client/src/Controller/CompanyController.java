/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Company;
import Model.Job;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * FXML Controller class
 *
 * @author DELL
 */
public class CompanyController implements Initializable {

    @FXML
    private TableView<Company> tableCompany;

    @FXML
    private TableColumn<Company, String> colName;

    @FXML
    private TableColumn<Company, String> colDescription;

    @FXML
    private TableColumn<Company, String> colLocation;

    @FXML
    private TableColumn<Company, Integer> colNumberEmp;

    @FXML
    private TableColumn<Company, String> colAvatar;

    @FXML
    private TableColumn<Company, String> colTypeOrg;

    @FXML
    private Button btnBack;

    @FXML
    void onClickBack(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent newScene = null;

        stage = (Stage) btnBack.getScene().getWindow();
        newScene = FXMLLoader.load(getClass().getResource("/Job.fxml"));

        Scene scene = new Scene(newScene);
        stage.setScene(scene);
        stage.setTitle("Công việc");
        stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        HttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:8881/company");
        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        
        HttpResponse response = null;
        try {
            response = httpclient.execute(httpGet);
        } catch (IOException ex) {
        }

        String content = null;
        try {
            content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
        } catch (IOException | UnsupportedOperationException ex) {
        }

        JSONObject responseData = new JSONObject(content);
        JSONArray arr = responseData.getJSONArray("dataList");
        List<Company> company = new ArrayList<Company>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            Company com = new Company();
            com.setName(obj.getString("name"));
            com.setCompanyAvatar(obj.getString("companyavatar"));
            com.setDescription(obj.getString("description"));
            com.setLocation(obj.getString("location"));
            com.setNumberEmp(obj.getInt("numberofemployees"));
            com.setTypeOrg(obj.getString("typeoforganization"));
            company.add(com);
        }
        ObservableList data = FXCollections.observableArrayList(company);
        
        colName.setCellValueFactory(new PropertyValueFactory<Company, String>("name"));
        colAvatar.setCellValueFactory(new PropertyValueFactory<Company, String>("companyavatar"));
        colDescription.setCellValueFactory(new PropertyValueFactory<Company, String>("description"));
        colLocation.setCellValueFactory(new PropertyValueFactory<Company, String>("location"));
        colNumberEmp.setCellValueFactory(new PropertyValueFactory<Company, Integer>("numberEmp"));
        colTypeOrg.setCellValueFactory(new PropertyValueFactory<Company, String>("typeOrg"));
        tableCompany.setItems(data);
    }

}
