package Controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import project2client.Project2Client;

public class LogInController {

    @FXML
    private AnchorPane pane;

    @FXML
    private TextField txtUsername;

    @FXML
    private TextField txtPassword;

    @FXML
    private Label txtMessage;

    @FXML
    private Button btnLogin;

    @FXML
    private Button btnSignup;

    private String username;
    private String password;

    public static String accessToken;

    @FXML
    void onClickLogin(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent newScene = null;

        username = txtUsername.getText();
        password = txtPassword.getText();
        String body = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\"}";
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8881/authentication/login");
        httpPost.setHeader("Content-type", "application/json");

        try {
            StringEntity stringEntity = new StringEntity(body);
            httpPost.setEntity(stringEntity);
        } catch (UnsupportedEncodingException ex) {
        }

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpPost);
        } catch (IOException ex) {
        }

        String content = null;
        try {
            content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
        } catch (IOException | UnsupportedOperationException ex) {
        }

        JSONObject responseData = new JSONObject(content);

        if (username.isEmpty() || password.isEmpty()) {
            txtMessage.setText("Tên đăng nhập hoặc mật khẩu không được để trống");
        } else {
            try {
                Boolean success = responseData.getBoolean("success");
                if (success) {
                    accessToken = responseData.getString("accessToken");
                    txtMessage.setText("Đăng nhập thành công");
                    String role = responseData.getString("roleName");
                    if ("EMPLOYER".equals(role)) {
                        stage = (Stage) btnLogin.getScene().getWindow();
                        newScene = FXMLLoader.load(getClass().getResource("/postJob.fxml"));

                        Scene scene = new Scene(newScene);
                        stage.setScene(scene);
                        stage.setTitle("Đăng tin tuyển dụng");
                        stage.show();
                    } else if ("JOB_SEEKER".equals(role)) {
                        stage = (Stage) btnLogin.getScene().getWindow();
                        newScene = FXMLLoader.load(getClass().getResource("/Job.fxml"));

                        Scene scene = new Scene(newScene);
                        stage.setScene(scene);
                        stage.setTitle("Công việc");
                        stage.show();
                    }

                } else {
                    try {
                        String message = responseData.getString("message");
                        if (message == "username or password is not match") {
                            txtMessage.setText("Tên đăng nhập hoặc mật khẩu không đúng");
                        }
                    } catch (Exception e) {
                    }
                }
            } catch (Exception e) {
                txtMessage.setText("Tên đăng nhập hoặc mật khẩu không đúng");
            }
        }
    }

    @FXML
    void onClickSignup(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent newScene = null;
        stage = (Stage) btnLogin.getScene().getWindow();
        newScene = FXMLLoader.load(getClass().getResource("/signUp.fxml"));

        Scene scene = new Scene(newScene);
        stage.setScene(scene);
        stage.setTitle("Job");
        stage.show();
    }

}
