/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

/**
 * FXML Controller class
 *
 * @author DELL
 */
public class SignUpController implements Initializable {

    @FXML
    private Button btnSignup;

    @FXML
    private Button btnBack;

    @FXML
    private TextField txtUsername;

    @FXML
    private TextField txtFirstname;

    @FXML
    private TextField txtLastname;

    @FXML
    private TextField txtRole;

    @FXML
    private TextField txtBirthday;

    @FXML
    private TextField txtEmail;

    @FXML
    private TextField txtPhone;

    @FXML
    private TextField txtAddress;

    @FXML
    private TextField txtCompanyname;

    @FXML
    private TextField txtNumberofEmp;

    @FXML
    private TextField txtTypeofOrg;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Label txtMessage;

    @FXML
    void onClickSignup(ActionEvent event) {
        String username = txtUsername.getText();
        String password = txtPassword.getText();
        String firstName = txtFirstname.getText();
        String lastName = txtLastname.getText();
        String role = txtRole.getText();
        String birthDay = txtBirthday.getText();
        String email = txtEmail.getText();
        String contact = txtPhone.getText();
        String companyName = txtCompanyname.getText();
        String address = txtAddress.getText();
        int number = !"".equals(txtNumberofEmp.getText()) ? Integer.parseInt(txtNumberofEmp.getText()) : 0;
        String typeOrganization = txtTypeofOrg.getText();

        String body = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\", \"firstName\": \"" + firstName + "\", \"lastName\": \"" + lastName + "\", \"roleName\": \"" + role + "\", \"birthDay\": \"" + birthDay + "\", \"email\": \"" + email + "\", \"contact\": \"" + contact + "\", \"companyName\": \"" + companyName + "\", \"address\": \"" + address + "\", \"numberofemployees\": \"" + number + "\", \"typeoforganization\": \"" + typeOrganization + "\"}";
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8881/authentication/signup");
        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

        try {
            StringEntity stringEntity = new StringEntity(body);
            httpPost.setEntity(stringEntity);
        } catch (UnsupportedEncodingException ex) {
        }

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpPost);
        } catch (IOException ex) {
        }

        String content = null;
        try {
            content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
        } catch (IOException | UnsupportedOperationException ex) {
        }

        JSONObject responseData = new JSONObject(content);

        if (username.isEmpty() || password.isEmpty()) {
            txtMessage.setText("Có lỗi xảy ra");
        } else {
            try {
                Boolean success = responseData.getBoolean("success");
                if (success) {
                    txtMessage.setText("Đăng ký thành công");
                } else {
                    try {
                        txtMessage.setText("Có lỗi xảy ra");
                    } catch (Exception e) {
                    }
                }
            } catch (Exception e) {
                txtMessage.setText("Có lỗi xảy ra");
            }
        }
    }

    @FXML
    void onClickBack(ActionEvent event) throws IOException {
        Stage stage = null;
        Parent newScene = null;

        stage = (Stage) btnBack.getScene().getWindow();
        newScene = FXMLLoader.load(getClass().getResource("/logIn.fxml"));

        Scene scene = new Scene(newScene);
        stage.setScene(scene);
        stage.setTitle("Đăng nhập");
        stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
