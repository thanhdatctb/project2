package Model;

import javafx.scene.control.Button;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DELL
 */
public class Job {
    private int id;
    private String title;
    private String category;
    private String salary;
    private String skill;
    private int experience;
    private String job_role;
    private String description;
    private String location;
    private String exp_date;
    private Button button;

    public Job(int id, String title, String category, String salary, String skill, int experience, String job_role, String description, String location, String exp_date, Button button) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.salary = salary;
        this.skill = skill;
        this.experience = experience;
        this.job_role = job_role;
        this.description = description;
        this.location = location;
        this.exp_date = exp_date;
        this.button = button;
        this.button.setText("Xem chi tiết");
    }
    
    public Job() {
        this.button = new Button("Xem chi tiết");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getJob_role() {
        return job_role;
    }

    public void setJob_role(String job_role) {
        this.job_role = job_role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
        this.button.setText("Xem chi tiết");
    }
}
