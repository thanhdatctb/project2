/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author DELL
 */
public class Company {
    private String name;
    private String companyAvatar;
    private String description;
    private String location;
    private int numberEmp;
    private String typeOrg;

    public Company(String name, String companyAvatar, String description, String location, int numberEmp, String typeOrg) {
        this.name = name;
        this.companyAvatar = companyAvatar;
        this.description = description;
        this.location = location;
        this.numberEmp = numberEmp;
        this.typeOrg = typeOrg;
    }

    public Company() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyAvatar() {
        return companyAvatar;
    }

    public void setCompanyAvatar(String companyAvatar) {
        this.companyAvatar = companyAvatar;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getNumberEmp() {
        return numberEmp;
    }

    public void setNumberEmp(int numberEmp) {
        this.numberEmp = numberEmp;
    }

    public String getTypeOrg() {
        return typeOrg;
    }

    public void setTypeOrg(String typeOrg) {
        this.typeOrg = typeOrg;
    }
    
    
    
}
